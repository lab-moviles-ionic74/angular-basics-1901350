import { PersonajeInputComponent } from './personajes/personaje-input.component';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { AppComponent } from './app.component';
import { PersonajesComponent } from './personajes/personajes.component';
import { AppRoutingModule } from './app-routing.module';
import { HttpClientModule } from '@angular/common/http';
@NgModule({
 declarations: [
 AppComponent,
 PersonajesComponent,//Aqui lo agregamos y arriba esta la dependencia
 PersonajeInputComponent
 ],
 imports: [
 BrowserModule,
 FormsModule,
 AppRoutingModule,
 HttpClientModule
 ],
 providers: [],
 bootstrap: [AppComponent]
})
export class AppModule { }
