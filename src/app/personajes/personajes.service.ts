import {Injectable} from '@angular/core';
import { Subject } from 'rxjs';
import {HttpClient} from '@angular/common/http';
import {map} from 'rxjs/operators';

@Injectable({providedIn: 'root'})
export class PersonajeService{
 personajes: string[] = ['Batman'];//Aqui declararemos el listado de nuestros personajes
 //Esa variable la usaremos para el cambio de estado de nuesto componente
 // Al declararlo como Subject podermos escuchar los cambios desde otros componentes
 personajesChange = new Subject<string[]>();

 constructor(private http: HttpClient){}

 fetchPersonajes(){
  this.http.get<any>('https://swapi.dev/api/people/')
  .pipe(map(response => {//aqui mapeamos la respuesta del servicio y solo obtenemos el campo "name"
  return response.results.map(obj => obj.name);
  }))
  .subscribe(response => {//aqui cuando se terine la consulta y el mapeo asigna los valores a nuestro arreglo
  console.warn(response);
  this.personajes = response;
  this.personajesChange.next(this.personajes);
  });
  }


 addPersonaje(name: string){//Metodo de alta de personaje
 this.personajes.push(name);
 this.personajesChange.next(this.personajes);
 }

 removePersonaje(name: string){//Metodo de baja de personaje
 this.personajes = this.personajes.filter(personaje => {//aplicamos un ltro para devolver todos los personajes menos el
 return personaje !== name;
 });
 this.personajesChange.next(this.personajes);
 }
}
