import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import { Subscription } from 'rxjs';
import { PersonajeService } from './personajes.service';

@Component({
 selector: 'app-personajes',//aqui designamos el tag de nuestro componente
 templateUrl: './personajes.component.html', //en este archivo ponemos la estructura de nuestro componente
 styleUrls: ['./personajes.component.css'] //aqui cargamos los estilos que utilizara nuestro componente
})
export class PersonajesComponent implements OnInit, OnDestroy{
  //declaramos una variable booleana para control de despliegue
  private personajesSubs: Subscription = new Subscription;

 activo: boolean = true;
 personajes: string[] = [];//Cambiamos a un arreglo de strings nuestra variable de personajes
 consultando: boolean = true;

//Declaramos nuestro constructor y como parametro ponemos una variable del tipo de nuetro Servicio
 // lo declaramos como privado para poder utilizarlo en toda nuestra clase
 constructor(private service: PersonajeService){}

 ngOnInit(){
  this.service.fetchPersonajes();//cambiamos el metodo del servicio por la consulta del api
  this.personajesSubs = this.service.personajesChange.subscribe(personajes => {
  this.personajes = personajes;
  this.consultando = false;//cambiamos el estus de la variable una vez que nos haya regresado la informacion
  });
  }

 ngOnDestroy(){
 //Implementamos este metodo para cuando nuestro componente no se
 // despliege deje de escuchar los cambios de nuestro servicio
 this.personajesSubs.unsubscribe();
 }

 //Declaramos este metodo para poder eliminar elementos de nuestro listado del servicio
 onRemovePersonaje(name: string){
 this.service.removePersonaje(name);
 }



 //Creamos un metodo que cambie el valor de nuestra bandera
 onClickActivar(){
  this.activo = !this.activo;
 }
}
